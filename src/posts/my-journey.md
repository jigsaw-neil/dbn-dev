---
title: 'My Journey'
metaDesc: 'In the immortal words of David Byrne ‘well, how did I get here?’'
summary: 'In the immortal words of David Byrne ‘well, how did I get here?’'
image: '/images/blog/david-byrne.gif'
date: '2020-11-28'
tags: ['Design', 'DBN']
---

My journey. Well, the first thing to say about it is it's not been particularly linear, though it's been all the more interesting for that to be honest. 

I began as a writer. That has ever been my single and most natural talent, right back to being a school kid. I've always been able to write well without knowing exactly what it was that made my writing decent or engaging, however it took me until the age of 21 to pursue it at university after a failed attempt at becoming an electronics engineer.

**N.B.** I didn't blow myself up - result.

After in turn deciding that I didn't want to be a civil servant/journalist/PR exec I finally found code through a company blog on WordPress that I was responsible for. I still remember working out how to change a link colour and *ping*, that was it. 

Given my lack of knowledge at that stage, what followed after that moment wasn't as profound as I might have hoped to be writing. Instead what followed were several jobs that carried varying levels of development work, underpinned by advertising and marketing which I knew inside out. But over time the balance changed until in the words of Yoda 'full-time dev I was'.

It's had it's good moments. It's also had its exasperated, completely stuck moments that consisted of more and more desperate Google searching and eventually screaming into a pillow and drinking. Depending of course, on how much of an existential crisis z-index or an overflowing element that I couldn't diagnose was giving me. 

But I enjoyed it. So I carried on, battled through and eventually began to feel a little more than totally useless (I know me quite well and that is a **WIN** people). 

One thing that has never left me is that **BUZZ** I get when I learn something new and again when I apply it somewhere for the first time. It's wonderful. In fact I imagine it's a bit like Cristiano Ronaldo and getting the winner in a big final say. Just without the bank balance. 

These days I have a particular love for Sass and an apprecation of just how fucking awesome (sorry mom) HTML is, plenty of WordPress/PHP knowledge and a growing interest in static site generators, as this site will attest to.

But mainly I work with people and businesses to give them exactly what they need from their website, marketing and copy. Even if it's not what they **thought** they needed when we first spoke. In need of help? Just follow the little paper plane and drop me a message.

One closing thought: would it be possible to find an appropriate Talking Heads gif for every post?