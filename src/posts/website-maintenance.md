---
title: 'The importance of website maintenance'
metaDesc: 'The importance of website maintenance and how to keep your site safe'
summary: 'How and why you should keep your site up to date.'
image: '/images/blog/how-this-site-was-built.gif'
date: '2021-01-15'
tags: ['tips and tricks', 'DBN']
---
## We love WordPress, but...

WordPress is a great tool. Whether used headless or end to end it's the ideal choice for most web projects, something which the numbers back up with stats showing that WordPress powers around one third of all websites. 

However it is also a popular hacker target, but why is this. Well stats also show that around 70% of all WordPress sites have issues that leave them vulnerable to some kind of attack. These issues can be out of date WordPress versions, out of date plugins, weak passwords and so on, so just update everything and choose a more secure password right?

Well, yes and no. Let me explain. 

### Conflict

Unfortunately in this world there is potential for conflict everywhere and your WordPress website is no exception. One of the great things about WordPress is that it is open source, meaning that 3rd party developers can make things (usually plugins or themes) for it. The downside of this is that these externals pieces of code can clash, either with core WordPress code or with one another, giving us in many cases the white screen of death and if we don't have file transfer protocol (FTP) access to the site files, no way of gaining access to correct it.

### Conundrum

So you need your site's core and 3rd party software to be up to date but updating it all might bring the whole thing crashing down.

I FEED STUCK conundrum to go here

### The solution 

The solution is to take a cautious and methodical approach to updating and to check each update in a test environment to ensure that it works and won't cause the dreaded screen of death. 

To do this you need to set up a test copy of your website, check of all the updates and, if there are no problems, push those updates to the live site. But you don't have time to that surely? You have a business to run after all.

### That's where I can help

GSAP and SVG is one of the great duos. Think Lennon and McCartney, Bowie and Iggy, Kylie and Jason. SVG is realtively new to me because, as I've pointed out elsewhere on this site my ability to draw anything is basically zero. All of that talent in my family went to my little brother. 

But through perseverance and plenty of YouTube hours I started to understand how to use basic shapes to create other, more complex ones and away I went. After that the only issue I faced was resisting the temptation to animate **EVERYTHING** on the site. 

### Benefits

Firstly there's an enormously reduced chance of hacking and the downtime that would result from that plus minimal chance of plugin or theme conflicts causing downtime and then the added bonus of improved user performance which more often than not leads to an noticable increase in conversions, be that sales, sign ups, submissions etc. 

Investing in your website maintenance helps ensure uptime in excess of 95% and leaves you free to get on with what you do best - running you business.

Why wouldn't you?

## Maintenance plans

|                         	| Standard £49 p/m 	| Pro £59 p/m 	| Business £79 p/m 	|
|-------------------------	|:----------------:	|:-----------:	|:----------------:	|
| Plugin/theme updates    	|      Monthly     	|  Bi-weekly  	|      Weekly      	|
| Core WordPress updates  	|      Monthly     	|  Bi-weekly  	|      Weekly      	|
| Daily backups           	|      &check;     	|   &check;   	|      &check;     	|
| SSL issue fixing        	|      &cross;     	|   &check;   	|      &check;     	|
| Hidden login page       	|      &cross;     	|   &cross;   	|      &check;     	|
| Sucuri security suite   	|      &cross;     	|   &cross;   	|      &check;     	|
| Annual payment discount 	|        5%        	|     10%     	|        15%       	|

Interested? Drop me a line at [howdy@developedbyneil.com](mailto:howdy@developedbyneil.com)