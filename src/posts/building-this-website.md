---
title: 'Building this website'
metaDesc: 'How the Developed by Neil site was born and what I used to make it.'
summary: 'How the Developed by Neil site was born and what I used to make it.'
image: '/images/blog/how-this-site-was-built.gif'
date: '2020-12-02'
tags: ['Design', 'DBN']
---
First thing to say about this site build is that it has been on the cards for a long time. So long in fact that the site for my previous incarnation Jigsaw Creative has seen no new content since about 2018.

I've been lucky in many ways in that I've been busy but the time for change had come and, much as I love WordPress, I wanted to embrace something new and Eleventy was it.

## Tools used

### Eleventy

With WordPress I had become more than a little frustrated with some of the accepted load speeds (something that I plan to solve in 2021 by learning how to use it as a headless CMS) and in my case I only needed something simple so it felt like the ideal time to dip my toe into the world of static site generators.

There was only one problem. I knew less about SSG's than Boris Johnson knows about telling the truth so it was an enormous stroke of fortune to stumble across the work of Andy Bell and his brilliant Eleventy from Scratch course on Twitter. A massive hat tip must go to him as that course is the cornerstone of my Eleventy learning. 

### Greensock (GSAP)

Aside from Eleventy my other major piece of progress in my own development in 2020 is that I have risen to the rank of 'not completely useless' when it comes to GSAP which is a wonderful Javascript framework for animation. Honestly, if you haven't tried it, stop reading immediately and do it now. Try the stagger function and count exactly how many keyframe animations it has saved you from writing. I've even found it useful in organising my menu animation for this site, just coupled to one click event listener. That's it.

### SVG

GSAP and SVG is one of the great duos. Think Lennon and McCartney, Bowie and Iggy, Kylie and Jason. SVG is realtively new to me because, as I've pointed out elsewhere on this site my ability to draw anything is basically zero. All of that talent in my family went to my little brother. 

But through perseverance and plenty of YouTube hours I started to understand how to use basic shapes to create other, more complex ones and away I went. After that the only issue I faced was resisting the temptation to animate **EVERYTHING** on the site. 

### Sass (SCSS)

Ah Sass, my old friend. I remember the days when had zero idea what an .scss file was or what it did. But what a wonderful piece of kit it is, both powerful and intuitive. Yes I know, it's 2020 and CSS Variables are cool (they really are), but I will always love Sass and it was invaluable in this project. As was anotter Sass-based tool that came bundled for free in Andy Bell's Eleventy from Scratch course, a wonderful little library called Gorko that brought design scales into my work.

If these are new to you as they were to me, design scales are sizing scales that help build a relationship between the sizing of the elements in your design, creating a natural flow.
