--- 
title: 'Blog'
metaDesc: 'This blog is where I offload my thoughts, so expect a mix of front end, good jokes, self care, mental health and unitentionally bad jokes.'
layout: 'layouts/blog.html'
pagination: 
    data: collections.blog
    size: 10
permalink: 'blog{% if pagination.pageNumber > 0 %}/page/{{ pagination.pageNumber }}{% endif %}/index.html'
paginationPrevText: 'Newer posts'
paginationNextText: 'Older posts'
paginationAnchor: '#post-list'
badJoke: 'What does a baby computer call its father? Data.'
---

This blog is where I offload my thoughts, so expect a mix of front end, good jokes, self care, mental health and unitentionally bad jokes. 