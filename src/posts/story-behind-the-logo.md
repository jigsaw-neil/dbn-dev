---
title: 'The story behind the logo'
summary: 'The local, historical landmark that inspired it.'
image: '/images/blog/baggeridge-brick-tower.jpg'
date: '2020-12-01'
tags: ['Design', 'DBN']
---

When it came to designing the logo for my website I had two problems. One, I had no idea what I wanted and two, the small matter of a lack of any kind of artistic ability. My lack of ability drawing with pencil and paperwork is one of the reasons I find the things GSAP lets me do to be borderline magical.

I began to look around for inspiration a few months ago and found absolutely nothing. Until one day I consciously noticed something that I am able to see from most vantage points in my local area (including my own back garden) - the now disused Baggeridge Brick tower.

Baggeridge Brick was founded in 1944 and was a major part of local industry until it was purchased in 2007 by the Austrian company Wienerberger AG who moved the UK headquarters to Manchester. Since then the brick tower has lain dormant but is a very visible part of the local landscape and as I wanted the logo to reflect the local area that I have lived in all my life, it felt like the perfect fit. Plus a rectangle with with one tapered end and rounded corners is something that even I could draw. I even gave it a cool shadow (and myself a GOLD star).

I would also love to tell you that I chose the tower as the logo because its history of industry and work also nicely reflect aspects of my own character, but in truth I only thought of that after I'd chosen it. 