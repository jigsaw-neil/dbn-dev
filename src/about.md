---
title: 'About'
metaDesc: 'Developed by Neil - About me. Find out about my journey, it’s not that boring, honest.'
layout: 'layouts/about.html'
summary: 'Find out about me and my journey. It’s not as boring as it sounds, honest.'
lifeProgressBar: '/images/about/life-progress-bar.svg'
meImage: '/images/about/me-reykjavik.jpg'
factfile: 
    born:       '1984'
    noTie:      '2006'
    degree:     '2010'
    codeStart:  '2011'
    pr:         '2013'
    codeFocus:  'later in 2013'
myStory: 
    storyTitle: 'My Story'
    paraOne: 'I’m a designer, developer and copywriter who started out in my working life as a wannabe journalist who instead found himself in PR.'
    paraTwo: 'It was there whilst happy with my ability to write and despondant at my lack of ability to sell, that I stumbled upon coding.'
    paraThree: 'I was handed the company WordPress blog to look after, managed to change the colour of a link to the company pink, and was hooked. That would be 2011.'
    paraFour: 'Ever since then things have been a self-taught journey of discovery, teaching myself to code and applying these skills to running the marketing and websites for an entertainments company and then on to setting up and managing a major high street retailers first foray into ecommerce.'
    paraFive: 'It wasn’t always fun but it was at the least very interesting but eventually the drive to work for myself became impossible to ignore.'
    paraSix: 'Since 2017 I’ve run DBN (formerly Jigsaw) helping clients of various sizes with their web presence, copywriting, advertising and design, largely remotely but I can sometimes be spotted on site in and around the West Midlands area.'

travel:
    travelTitle: 'Travel'
    planeImage: '/images/about/aeroplane.svg'
    paraOne: 'Away from work my partner and I love to travel and have been lucky enough to see New York, western Canada, Iceland, Norway, Finland and more than a little of Italy in the last few years.'
    paraTwo: 'If we’re ever allowed to travel again Greenland, Australia and New Zealand top the list.'

music: 
    musicTitle: 'Music'
    musicImage: '/images/about/vinyl-record.svg'
    paraOne: 'Yep. I’m that guy with all the vinyl records (250 and rapidly growing at last count).'
    paraTwo: 'It’s something that took me years to get into having sold all of my CD’s and gone digital but I love listening to a proper vinyl. There’s nothing like it. I’ll get down off my soapbox now...'

animals: 
    animalsTitle: 'Animals'
    charlieImage: '/images/about/charlie-dog.svg'
    paraOne: 'My best little mate is my dog Charlie. For almost a decade he’s been the most loyal, lovably daft companion that you could have.'    
    paraTwo: 'You can see more from him in amongst pictures of records and cans of ale on my instagram below.'

buttonText: 'Dogs ‘n’ Records'

---