---
title: 'Welcome to Designed By Neil'
metaDesc: 'Hi, I’m Neil, freelance developer, designer and copywriter in Sedgley, West Midlands.'
layout: 'layouts/home.html'
intro:
  mainOne: 'Website Design'
  mainTwo: 'Copywriting'
  mainThree: 'Digital Advertising'
  speechBubbleText: 'Hi, I’m Neil, freelance developer, designer and copywriter in Sedgley, West Midlands.'
workWithMe: 
    title: 'Work With Me'
    text: 'I handle website designs, builds, copywriting and advertising for clients of various sizes, details of which are below. Or you can get in touch now, it costs nothing to have a conversation and might just save you a fortune.'
    contactBtnText: 'Get In Touch'
    contactBtnUrl: 'mailto:howdy@developedbyneil.com'
webDesign:
    title: 'Website Design'
    text: 'Whether you are looking for a custom WordPress theme build, ecommerce store or a marketing-led site designed to generate leads, you come to the right place, and I can help with SEO.'
copywriting: 
    title: 'Copywriting'
    text: 'Tell the story of your business and why potential customers should love you. You will likely only get one chance to grab their attention so you need to get it right. Let a man who was two minutes away from being printed in the Financial Times help. Just do not ask about that story. Still hurts.'
advertising: 
    title: 'Advertising'    
    text: 'Get help getting the most from your advertising budget on AdWords, Google PLAs (formerly Google Shopping), Facebook, Instagram and Pinterest plus remarketing on all platforms.'
---