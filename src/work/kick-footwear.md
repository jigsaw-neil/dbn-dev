---
title: 'Kick Footwear'
metaDesc: 'Kick Footwear - Google Shopping/PLAs, social media advertising and email marketing.'
summary: 'Google Shopping/PLAs, social media advertising and email marketing.'
image: '/images/work/kick-footwear.png'
icon: '/images/work/logos/kick-footwear-logo.png'
websiteURL: 'https://www.kickfootwear.co.uk'
tags: [ 'Google AdWords', 'Google Shopping', 'Social Media Advertising', 'Facebook Advertising', 'Instagram Advertising', 'Ecommerce', 'Shopify' ]
displayOrder: 6
---

At the time of writing I have been working with Kick Footwear for approching two years, during which time I have organised a wide range of online advertising for their Shopify-powered store.

This covers Google PLA's (formerly Google Shopping), Google Display Ads, Facebook advertising and several promoted stories and products on Instagram. Consistently these ads have driven sales at a more than profitable rate for the business, underpinning and supporting the other arm of their ecommerce which is powered through the Amazon Marketplace.