---
title: 'Shakespeare4x4'
metaDesc: 'Shakespeare4x4 - Website build and WordPress web hosting.'
summary: 'Website build and WordPress web hosting.'
image: '/images/work/shakespeare4x4.png'
icon: '/images/work/shakespeare4x4.png'
websiteURL: 'https://www.shakespeare4x4.co.uk'
tags: [ 'Web Design', 'Web Hosting', 'Email Hosting', 'WordPress Maintenance']
displayOrder: 9
---

Shakespeare 4×4 Centre are an established family motoring firm that has been selling 4×4 and luxury vehicles for many years. They apprached me expressing dissatisfaction with the cost and functionality of their existing Auto Trader-powered setup and were looking to break away and have a bespoke solution built.

After an initial discussion we laid out their project needs as a blueprint to work from, the site needed to be:

* Easy to navigate
* Visually attraactive
* Get across the business values
* Have custom ‘car’ fields created to allow the business to add new vehicles for sale
* Have a bespoke search facility in order to allow customers to search those vehicles for make, engine size, colour, fuel type, number of doors and so on.

**What We Did**

First of all we put in the framework for the site, ensuring that it was easy to navigate and visually appealing to potential customers, using chevron detailing to show and hide certain elements of image and convey that feeling of movement and motion that we all associate with vehicles.

 Next came the custom car posts. Created with Advanced Custom Fields with a Woo Flexslider these included all of the details required by the client, as well as clear vehicle photography with a backend interface that makes it intuitive to add a new vehicle for sale.

Next came the most complex, and important, part of the build – the custom search facility. Using a combination of PHP and Javascript we put in place a system that queries the custom vehicle posts and returns the vehicles that match the search criteria, making it easy for customers to find what they want and enquire about the vehicle in a matter of clicks.

The feedback from the client was overwhemingly positive:

“Neil completed a beautiful new website for all complete with bespoke car creator and search facilities that finally allowed us to break away from the expense and inconvenience of an Auto Trader powered-website. It’s really helping our business become more efficient and profitable.” – **Emma Campbell, Director.**
