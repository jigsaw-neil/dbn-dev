---
title: 'Work'
layout: 'layouts/work.html'
pagination: 
    data: collections.work
    size: 9
permalink: 'work{% if pagination.pageNumber > 0 %}/page/{{ pagination.pageNumber }}{% endif %}/index.html'
paginationPrevText: 'Recent work'
paginationNextText: 'Previous work'
paginationAnchor: '#work-list'
---