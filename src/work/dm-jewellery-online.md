---
title: 'DM Jewellery Online'
metaDesc: 'WordPress web hosting and social media advertising.'
summary: 'WordPress web hosting and social media advertising.'
image: '/images/work/dm-jewellery-online.png'
icon: '/images/work/logos/dm-jewellery-logo.png'
websiteURL: 'https://www.dmjewelleryonline.co.uk/'
tags: [ 'Website Migrations', 'Web Design', 'Web Hosting', 'Email Hosting' ]
displayOrder: 8
---

Sharan from DM Jewellery online approached me having grown dissatisfied with the agency in India that she had commissioned to complete her new website build. Upon gaining access to the site and migrating it to my hosting we set about making improvements to it so that it really refelcted teh vision that Sharan had originally had for it.

Once this had been done we set about taking it to market with advertising campaigns across social media platforms to drive interest and there are plans to launch the product range on the Google Shopping platform in 2021.

For now though the business is up and running, the SSD-powered hosting ensures that it is consistently available and loads well so the initial steps to success have been taken.