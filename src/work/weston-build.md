---
title: 'Weston Build'
metaDesc: 'Weston Build - How I helped Weston Build create their new website'
summary: 'How I helped Weston Build create their new website'
image: '/images/work/weston-build.png'
icon: '/images/work/logos/weston-build-logo.jpg'
websiteURL: 'https://westonbuild.co.uk/'
tags: ['Web Design', 'Web Hosting']
displayOrder: 3
---

Weston Build are a Birmingham-based construction company that specialise in beautiful home extensions and conversions. At the point they approached me for help the business was progressing nicely through word of mouth, but they had come to realise that to maximise their potential they need a visually-striking site design and easy user experience that allowed them to showcase the quality of their previous works and encouraged customers to get in touch. 

The site build using the near-ubiquitous Divi theme as the client liked the idea of having a drag and drop builder available to them to add blog posts and work items in the future, so that's what we went with.

The finalised design made the most of the company's colour scheme to create a bold, vibrant site that (hopefully) sells the idea of the improved lifestyle to potential customers.

The company also took advantage of my powerful SSD-based hosting for £12pm or £120 per year.

You can see the final site below: