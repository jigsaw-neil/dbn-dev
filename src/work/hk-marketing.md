---
title: 'HK Marketing'
metaDesc: 'HK Marketing - Full site build, bespoke WordPress theme and web hosting.'
summary: 'Full site build, bespoke WordPress theme and web hosting.'
image: '/images/work/hk-marketing.png'
icon: '/images/work/logos/hk-marketing.png'
websiteURL: 'https://www.hkmarketing.co.uk'
tags: [ 'Web Design', 'Web Hosting', 'Email Hosting' ]
displayOrder: 5
---

For HK Marketing the brief was sinple, a nice, clean, professional looking site to help Hannah go full on freelance with her marketing business for the first time in her career.

Hannah had a clear idea that she wanted the site to be kept simple but to retain some colour and fun (hence the colourful services blocks). To help keep it simepl we decided on a single page design that helped illustrate who Hannah is and what her particular set of skills are and also made it straightforward for people to get in touch with her via the site.

Finally to keep both the site and her emails reliable and in robust health Hannah took advantange of the opportunity to host both of them with me for £12pm.