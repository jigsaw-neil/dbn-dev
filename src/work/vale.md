---
title: 'Vale Fire and Security'
metaDesc: 'Vale Fire and Security - How I helped Vale Fire and Security with their new website'
summary: 'How I helped Vale Fire and Security with their new website'
image: '/images/work/vale-fire-and-security.png'
icon: '/images/work/logos/vale-logo.png'
websiteURL: 'https://www.valefireandsecurity.co.uk'
tags: [ 'Web Design' ]
displayOrder: 2
---

Tony Jenkins and the team at Vale had had some website troubles, culminating in their expiring Wix site having to be hurredly copied across to WordPress, resulting in a site that functioned ok, but was in no way user friendly.

It was at that point that the company made contact with me as they were looking for a more modern design that better expressed who the company was and allowed customers to find the products and services that they needed easily.

The scope of the project was a fresh visual design and bespoke WordPress theme from scratch. A website prototype was created first in Figma and once approved by Tony, the finalised build began. Based on a combination of Advanced Custom Fields, Yoast, the old faithful Contact Form 7 and a sprinkling of things like Flexslider, the final site is a clean, easy to navigate design that utilises the bold company colours. You check it out below: