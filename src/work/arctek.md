---
title: 'Arctek'
metaDesc: 'Website build and WordPress web hosting.'
summary: 'Website build and WordPress web hosting.'
image: '/images/work/arctek.png'
icon: '/images/work/logos/arctek-logo.png'
websiteURL: 'https://www.arctek.co.uk/'
tags: [ 'Web Design', 'Web Hosting' ]
displayOrder: 7
---



Arctek are an experienced and highly professional architects firm based in central Birmingham who undertake work for both residential and commercial clients. They approached me as they needed a web presence that reflected their standing and professionalism but that also made it easy for customers to contact them about specific projects. 

To acheive this we sat down with the guys from Arctek to get an aunderstading of the ideal customer journey from their point of view and set out to build a website that followed that path.

The end result is a beautiful, clean site that takes advantage of the impressive project images the company has to put across that professional image and enough information for potential customers to develop trust in Arctek and want to contact them, but not so much as to be confusing.

The feedback from Arctek was very positive and the site, coupled with an ongoing Google Adwords campaign, Google My Business listing and our fast SSD-based hosting, is the major tool in helping take the company to the next level.