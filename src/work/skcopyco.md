---
title: 'SK Copy Co'
metaDesc: 'SK Copy Co - Brand new WordPress theme, on page SEO, web hosting.'
summary: 'Brand new WordPress theme, on page SEO, web hosting.'
image: '/images/work/sk-copy-co.png'
icon: '/images/work/logos/sk-copy-co-logo.png'
websiteURL: 'https://www.skcopyco.com'
tags: ['Website Migrations', 'Web Hosting', 'Web Design' ]
displayOrder: 1
---

Sanina Kaur, the owner of SK Copy Co is someone that I've known for many years and, in what feels like a previous life, worked along side in the Public Relations sector. She came to me initially wanting to migrate her existing WordPress site to my hosting but this eventually evolved into wanting the look and feel of her site recreated using more accessible tools.

The major reason was that the theme her site was based on didn't play nicely with the Yoast SEO plugin being used to control on-page SEO which was a big no for Sanina. 

To create the site she wanted I built a custom theme from scratch using Advanced Custom Fields for content control and templating and managed to improve performance massively by cutting the number of plugins in use by almost 40%. 

The end product carried a slightly tweaked design including the lovely front page video and the beautiful face of yours truly looking through the Berlin Wall on the services page as I'm listed as the go to web design guy (blushes).

You can see the final website below: