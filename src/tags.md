---
title: 'Work Archive'
layout: 'layouts/work-tag.html'
pagination:
  data: collections
  size: 1
  alias: tag
  filter: ['all', 'design', 'blog', 'work', 'dbn', 'tips and tricks', 'rss']
permalink: '/tag/{{ tag | slug }}/'
---